const router = require("express").Router();
const UserController = require("../../controllers/api/userController");
const userController = new UserController();

router.get("/", userController.getUser);
router.post("/add", userController.insertUser);
router.put("/edit/:id", userController.updateUser);
router.delete("/delete/:id", userController.deleteUser);

module.exports = router;
